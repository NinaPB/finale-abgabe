from django.db import models

# Create your models here.
class Weekdays(models.Model):
    name = models.CharField(max_length=50)
    order = models.IntegerField()
    def __str__(self):
        return f'{self.name}'
        
class Genre(models.Model):
    genre_name = models.CharField(max_length=20)
    def __str__(self):
        return f'{self.genre_name}'
        
class Movie(models.Model):
    name = models.CharField(max_length=100)
    poster = models.CharField(max_length=300, null=True, blank=True)
    genre = models.ForeignKey(Genre, on_delete = models.CASCADE, null = True, blank = True)
    age = models.IntegerField(default=0, null=True, blank=True)
    duration = models.CharField(max_length=20, null=True, blank=True)
    description = models.TextField(null=True, blank=True)
    weekday = models.ForeignKey(Weekdays, on_delete = models.CASCADE, null = True, blank = True)
    rating = models.FloatField(null = True, blank = True)
    releasedate = models.CharField(max_length=20, null = True, blank = True)
    votes = models.IntegerField(default = 0, null = True, blank = True)
    
    def __str__(self):
        try:
            return f'{self.name, self.genre, self.age, self.duration, self.weekday.name, self.rating, self.releasedate, self.votes}'
        except AttributeError:
            return f'{self.name, self.genre, self.age, self.duration, self.rating, self.releasedate, self.votes}'
        
        
class Recommendation(models.Model):
    name = models.CharField(max_length=100)
    poster = models.CharField(max_length=300, null=True, blank=True)
    age = models.IntegerField(default=0, null=True, blank=True)
    duration = models.CharField(max_length=20, null=True, blank=True)
    description = models.TextField(null=True, blank=True)
    rating = models.FloatField(null = True, blank = True)
    releasedate = models.CharField(max_length=40, null = True, blank = True)
    search = models.CharField(max_length=100)
    votes = models.IntegerField(default = 0, null = True, blank = True)
    def __str__(self):
        return f'{self.name, self.votes}'
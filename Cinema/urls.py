from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('recommend', views.recommend, name='recommend'),
    path('vote', views.vote_recommend, name="vote"),
    path('current', views.current, name="current"),
    path('admin', views.admin, name="admin"),
]
from django.contrib import admin

# Register your models here.
from .models import Movie, Recommendation, Weekdays, Genre


class MovieAdmin(admin.ModelAdmin):
    list_display = ['weekday','name', 'genre','votes','age','duration','rating', 'releasedate']
    list_filter = ['weekday','age','votes', 'genre']
admin.site.register(Movie, MovieAdmin)
class WeekdaysAdmin(admin.ModelAdmin):
    list_display = ['name']
admin.site.register(Weekdays, WeekdaysAdmin)
class RecommendationAdmin(admin.ModelAdmin):
    list_display = ['name', 'search', 'votes','duration', 'rating', 'releasedate']
    list_filter = ['votes', 'rating', 'age']
admin.site.register(Recommendation, RecommendationAdmin)
admin.site.register(Genre)


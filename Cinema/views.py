from django.shortcuts import render

# Create your views here.
from django.shortcuts import get_object_or_404, render, redirect
from django.http import HttpResponse, HttpResponseRedirect
from django.urls import reverse
from .models import Movie, Recommendation, Weekdays, Genre

from bs4 import BeautifulSoup
import requests
from datetime import datetime
import re

def vote(request, movies):
    form = request.POST
    print(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")
    print(form)
    for item in movies:
        if item.name in form:
            item.votes += 1
            item.save()
    
def index(request):
    movies = Movie.objects.order_by('weekday')
    weekdays = Weekdays.objects.all()
    if request.method == 'POST':
        vote(request, movies)   
    return render(request, 'Cinema/index.html', {'movies': movies, 'weekdays': weekdays})
    
def get_imdb_data(imdb_id, search, model):
    #https://www.imdb.com/title/tt0110912/?ref_=adv_li_tt
    url = f"https://www.imdb.com/title/{imdb_id}/?ref_=adv_li_tt"
    response = requests.get(url)
    soup = BeautifulSoup(response.text, 'html.parser')
    #https://stackoverflow.com/questions/54098316/scrape-img-src-under-div-tag-using-beautifulsoup
    #for span_tag in soup.findAll('span'):
    #    span_tag.replace_with('')
        
    name = soup.select_one('div.title_wrapper').h1
    #https://stackoverflow.com/questions/22496822/removing-span-tags-from-soup-beautifulsoup-python
    for span_tag in name.findAll('span'):
        span_tag.replace_with('')
        
    name = " ".join(str(name).split())
    name = name.replace('<h1 class="">', '')
    name = name.replace(' </h1>', '')
    
    div = soup.find('div', {'class': 'poster'})
    poster = div.find('img').attrs['src']
    
    age = soup.select_one('div.subtext').find(text=True)
    age = re.sub(' +', '', age)

    duration = soup.select_one('div.subtext')    
    duration = str(duration.find('time').text)
    print(len(duration))
    
    #https://stackoverflow.com/questions/1546226/is-there-a-simple-way-to-remove-multiple-spaces-in-a-string
    #duration = re.sub(' +', '',duration) 
    duration = duration.strip()
    #https://stackoverflow.com/questions/42657894/beautifulsoup-scrape-itemprop-name-in-python
    rating = str(soup.find('span', itemprop="ratingValue"))
    rating = rating.replace('</span>', '')
    rating = float(rating.replace('<span itemprop="ratingValue">', ''))
    
    releasedate = soup.select_one('div.subtext')    
    releasedate = str(releasedate.find_all('a')[-1].text)
    releasedate = releasedate.strip()

    
    #https://dev.to/magesh236/scrape-imdb-movie-rating-and-details-3a7c
    #Konnte nur die englische Beschreibung scrapen, da die Deutsche durch JavaScript nachgeladen wurde und nur mit imdb pro Zugang zugänglich ist
    description = soup.find("div",{'class':'summary_text'}).text.strip()

    
    print('Name:',name, ', Rating:',rating)
    print('release',releasedate)
    try:
        model.objects.create(name=name,poster=poster, age=age, duration=duration, description=description, rating=rating, releasedate=releasedate, search=search)
    except TypeError: #falls es kein Feld für den Suchbegriff gibt
        model.objects.create(name=name,poster=poster, age=age, duration=duration, description=description, rating=rating, releasedate=releasedate)
    
    pass
    
def get_imdb_id(filmtitel):
    #https://www.imdb.com/find?q=pulp+fiction&ref_=nv_sr_sm
    
    #Schritt 1: Schauen ob es Treffer zum Filmtitel gibt
    # und wie die imdb-ID lautet
    
    filmtitel = filmtitel.replace(" ", "+") #In der URL werden Leerzeichen durch + dargestellt
    url = f"https://www.imdb.com/search/title/?title={filmtitel}"
    response = requests.get(url)
    
    soup = BeautifulSoup(response.text, 'html.parser')
    result = soup.select_one('div.ribbonize')
    if result == None:
        print(f"Keine Ergebnisse bei imdb für den gesuchten Titel")
        return None #Gib HTTP mit "Leider konnten wir keinen Film mit diesem Namen finden." aus
    id = result.attrs['data-tconst']
    
    print(f"ID für Film {filmtitel} : {id}")
    return id
    
def recommend(request):
    #Formular zur manuellen Eingabe
    genres = Genre.objects.order_by('genre_name')
    search = []
    
    if request.method == 'POST':
        if 'speichern' in request.POST:
            name = request.POST['name']
            Recommendation.objects.create(name=name)
            
            return render(request, 'Cinema/recommend.html', {'genres': genres})
        if 'vorschlagsuche' in request.POST:
            imdb_id = get_imdb_id(request.POST['vorschlag'])
            if imdb_id:
                search = str(request.POST['vorschlag'])
                get_imdb_data(imdb_id, search, Recommendation)
            
    return render(request, 'Cinema/recommend.html', {'genres': genres, 'search':search})
    
def vote_recommend(request):
    movies_recomm = Recommendation.objects.all()
    if request.method == 'POST':
        vote(request, movies_recomm)
    return render(request, 'Cinema/vote.html', {'movies_recomm': movies_recomm})
    
def current(request):
    movies = Movie.objects.order_by('weekday')
    weekdays = Weekdays.objects.all()
    now = datetime.now()
    plan = []
    for day in weekdays:
        max_value = 0
        for movie in movies:
            if movie.weekday.name == day.name:
                if movie.votes > max_value:
                    max_value = movie.votes
                else:
                    pass
                    
        chosen = movies.filter(votes=max_value)
        chosen = chosen.filter(weekday = day.id)
        plan.append(chosen)
        
        
    plan = str(plan)
    print(plan, type(plan))
    print('---')
    return render(request, 'Cinema/current.html', {'movies': movies, 'weekdays':weekdays, 'plan':plan})  
    
def admin(request):
    now = datetime.now()
    if 'my_movies' in request.POST:
        all_movies = Movie.objects.all()
        dateiname = 'my_movies'
        getfile(all_movies, now, dateiname)
    if 'recommendations' in request.POST:
        recommend = Recommendation.objects.all()
        dateiname = 'recommendations'
        getfile(recommend, now, dateiname)
    if 'vorschlagsuche' in request.POST:
        imdb_id = get_imdb_id(request.POST['vorschlag'])
        if imdb_id:
            search = str(request.POST['vorschlag'])
            get_imdb_data(imdb_id, search, Movie)
        
    return render(request, 'Cinema/admin.html')    

def getfile(movies, date, dateiname):  
    datei = open(dateiname + '.csv', 'a', newline='')
    for movie in movies:
        datei.write(movie.name)
        datei.write(';')
        try:
            datei.write(movie.genre.genre_name)
            datei.write(';')
        except AttributeError:
            pass
        datei.write(str(movie.age))
        datei.write(';')
        
        datei.write(movie.duration)
        
        datei.write(';')
        try:
            datei.write(movie.weekday.name)
            datei.write(';')
        except AttributeError:
            pass
        datei.write(str(movie.rating))
        datei.write(';')
        datei.write(movie.releasedate)
        
        datei.write(';')
        try:
            datei.write(movie.search)
            datei.write(';')
        except AttributeError:
            pass
        datei.write(str(movie.votes))
        datei.write(';')
        datei.write(str(date))
        datei.write(';')
    
        datei.write('\n')
    datei.close()